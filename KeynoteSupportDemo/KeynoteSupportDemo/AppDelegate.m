//
//  AppDelegate.m
//  KeynoteSupportDemo
//
//  Created by Stanislav Smida on 3/15/13.
//  Copyright (c) 2013 Stanislav Smida. All rights reserved.
//

#import "AppDelegate.h"
#import "Keynote.h"
#import <ScriptingBridge/ScriptingBridge.h>

@interface AppDelegate () <SBApplicationDelegate>

@end

@implementation AppDelegate

#pragma mark - NSApplicationDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Make sure you have Keynote installed.
    
    KeynoteApplication *application = (KeynoteApplication *)[SBApplication applicationWithBundleIdentifier:@"com.apple.iWork.Keynote"];
    application.delegate = self;
    
    // Open some presentation... Make sure the presentation exists at given URL.
    
    NSURL *keynoteURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask].lastObject URLByAppendingPathComponent:@"presentation.key"];
    KeynoteSlideshow *slideshow = [(KeynoteApplication *)application open:keynoteURL];
        
    // Here you can see that we are able to add object to slide.
    
    NSURL *imageURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask].lastObject URLByAppendingPathComponent:@"image.png"];
    [slideshow.currentSlide addFilePath:imageURL.path];
    
    // And here comes the problem. Generated Keynote.h says to pass invalid object types. Pass array of strings was pretty easy to change, but I have no idea how to pass valid data.
    
    NSInteger data[1][1] = {{1}};
    
    // I've also tried to pass dynamic array
    
//    NSInteger **data;
//    data = (NSInteger **)malloc(sizeof(NSInteger *));
//    data[0] = (NSInteger *)malloc(sizeof(NSInteger));
//    data[0][0] = 1;
    
    // and also
    
//    NSArray *data = @[@[@(1)]];

    [slideshow.currentSlide addChartColumnNames:@[@"column0"] data:data groupBy:KeynoteKCgbRow rowNames:@[@"row0"] type:KeynoteKCctHorizontal_bar_2d];
}

#pragma mark - SBApplicationDelegate

- (id)eventDidFail:(const AppleEvent *)event withError:(NSError *)error
{
    NSLog(@"%@", error);
    return nil;
}

@end
