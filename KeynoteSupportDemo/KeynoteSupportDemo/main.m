//
//  main.m
//  KeynoteSupportDemo
//
//  Created by Stanislav Smida on 3/15/13.
//  Copyright (c) 2013 Stanislav Smida. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
